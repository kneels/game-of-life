const genLabel = document.getElementById("generation-label");
const fpsLabel = document.getElementById("fps-label");
const fillLabel = document.getElementById("fill-label");
const canvas = document.getElementById("canvas");
const paintSelector = document.getElementById("paint-select");
const speedSlider = document.getElementById('speed-slider');
const fillSlider = document.getElementById('fill-slider');
const btnStartStop = document.getElementById('btn-start-stop');
const btnSeed = document.getElementById('btn-seed');
const btnReset = document.getElementById('btn-reset');
const ctx = canvas.getContext("2d");

let isPlaying = false;
let generation = 0;
let h = canvas.height;
let w = canvas.width;
const canvasScale = 10;
let anim;
let ticks = 0;
let seedPercentage;
let fps;
let grid = [[]];
let paintSelection = [[]];
const patterns = {
	"Glider": [
		[0, 0, 1],
		[1, 0, 1],
		[0, 1, 1],
	],
	"Light-weight spaceship": [
		[1, 0, 0, 1, 0],
		[0, 0, 0, 0, 1],
		[1, 0, 0, 0, 1],
		[0, 1, 1, 1, 1]
	],
	"Middle-weight spaceship": [
		[0, 1, 1, 1, 1, 1],
		[1, 0, 0, 0, 0, 1],
		[0, 0, 0, 0, 0, 1],
		[1, 0, 0, 0, 1, 0],
		[0, 0, 1, 0, 0, 0]
	],
	"Heavy-weight spaceship": [
		[0, 0, 1, 1, 0, 0, 0],
		[1, 0, 0, 0, 0, 1, 0],
		[0, 0, 0, 0, 0, 0, 1],
		[1, 0, 0, 0, 0, 0, 1],
		[0, 1, 1, 1, 1, 1, 1]
	],
	"Pulsar": [
		[0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1],
		[1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1],
		[1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1],
		[0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0],
		[1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1],
		[1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1],
		[1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1],
		[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0],
	],
	"Add pixel": [[1]],
	"Remove pixel": [[0]]
}

function drawPoint(x, y) {
	ctx.fillRect(x * canvasScale, y * canvasScale, canvasScale, canvasScale);
}

function seedGrid() {
	for (let x = 0; x < w; ++x) {
		for (let y = 0; y < h; ++y) {
			grid[x][y] = Math.random() < seedPercentage;
		}
	}
}

function render() {
	canvas.width = 0;
	canvas.width = w * canvasScale;
	for (let x = 0; x < w; ++x) {
		for (let y = 0; y < h; ++y) {
			if (grid[x][y]) {
				drawPoint(x, y);
			}
		}
	}
	genLabel.innerHTML = generation;
}

function getNeighbourCount(x, y, grid) {
	let count = 0;
	for (let i = x-1; i <= x + 1; ++i) {
		for (let j = y-1; j <= y + 1; ++j) {
			if (i === x && j === y) { continue; }
			let a = i;
			let b = j;

			if (a < 0) { 
				a += w; 
			}
			else if (a >= w) { 
				a = w - a;
			}
			if (b < 0) { 
				b += h; 
			}
			else if (b >= h) { 
				b = h - b;
			}

			if (grid[a][b]) {
				count++;
			}
		}
	}

	return count;
}

// Any live cell with two or three neighbors survives.
// Any dead cell with three live neighbors becomes a live cell.
// All other live cells die in the next generation. Similarly, all other dead cells stay dead.
function nextGeneration(grid) {
	let gridClone = grid.map(function(arr) {
	    return arr.slice();
	});

	for (let x = 0; x < w; ++x) {
		for (let y = 0; y < h; ++y) {
			let c = getNeighbourCount(x, y, grid);
			if (c !== 2 && c !== 3) {
				gridClone[x][y] = 0;
			}
			if (c === 3) { // birth
				gridClone[x][y] = 1;
			}
		}
	}
	return gridClone;
}

function update() {
	if (++ticks % parseInt(60 / fps) == 0) {
        grid = nextGeneration(grid);
		++generation;
		render(grid);
	}

	anim = requestAnimationFrame(update);
}

function start() {
	anim = requestAnimationFrame(update);
	isPlaying = true;
	btnStartStop.innerHTML = "❚❚";
}

function stop() {
	cancelAnimationFrame(anim);
	isPlaying = false;
	btnStartStop.innerHTML = "▶";
}

function reset() {
	for (let x = 0; x < w; ++x) {
		for (let y = 0; y < h; ++y) {
			grid[x][y] = 0;
		}
	}
	generation = 0;
}

function setFPS(newFPS) {
	fps = newFPS; 
	speedSlider.value = fps;
	fpsLabel.innerHTML = fps;
}

function setSeedPercentage(newSeedPercentage) {
	seedPercentage = newSeedPercentage; 
	fillSlider.value = seedPercentage * 100;
	fillLabel.innerHTML = seedPercentage.toFixed(2);
}

function getGridCoords(abs_pos) {
	return {
		x: Math.floor((abs_pos.x - canvas.offsetLeft) / canvasScale),
		y: Math.floor((abs_pos.y - canvas.offsetTop) / canvasScale)
	};
}

function addPattern(pos) {
	for (let row = 0; row < paintSelection.length; ++row) {
		for (let col = 0; col < paintSelection[row].length; ++col) {
			let x = (col + pos.x) % w;
			let y = (row + pos.y) % h;
			grid[x][y] = paintSelection[row][col];
		}
	}
}

function setPaintSelection() {
	paintSelection = patterns[paintSelector.value];
}

function resize(canvas) {
	// Lookup the size the browser is displaying the canvas.
	var displayWidth  = canvas.clientWidth;
	var displayHeight = canvas.clientHeight;

	// Check if the canvas is not the same size.
	if (canvas.width  != displayWidth ||
		canvas.height != displayHeight) {

		// Make the canvas the same size
		canvas.width  = displayWidth;
		canvas.height = displayHeight;
	}
}

function init() {
	stop();
	
	resize(canvas);
	h = Math.floor(canvas.height / canvasScale);
	w = Math.floor(canvas.width / canvasScale);
	
	grid = new Array(w);
	for (let x = 0; x < w; ++x) {
		grid[x] = new Array(h);
	}
	
	for (let key in patterns) {
		let option = document.createElement("option");
		option.text = key;
		paintSelector.add(option, paintSelector[0]);
	}

	generation = 0;
	
	setPaintSelection();
}

canvas.addEventListener('click', function(e) {
	const pos = {
		x: e.clientX,
		y: e.clientY
	};
	addPattern(getGridCoords(pos));
	render();
 }, false);
 

paintSelector.addEventListener('change', setPaintSelection);
btnStartStop.addEventListener('click', function() {
	if (isPlaying) {
		stop();
	} else {
		start();
	}
});
btnSeed.addEventListener('click', function() { seedGrid(); render(); });
btnReset.addEventListener('click', function() { stop(); reset(); render(); });
speedSlider.addEventListener('change', function() {	setFPS(speedSlider.value); });
fillSlider.addEventListener('change', function() {	setSeedPercentage(fillSlider.value / 100); });


window.onresize = function(e) {
	stop();
	reset();
	render();
}

window.onload = function(e) { 
	setFPS(15);
	setSeedPercentage(0.1);
	init();
}
